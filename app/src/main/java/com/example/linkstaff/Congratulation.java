package com.example.linkstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Congratulation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulation);
    }

    public void congratulation(View view) {
        Intent intent = new Intent(this, Profile_1.class);
        startActivity(intent);
    }
}
