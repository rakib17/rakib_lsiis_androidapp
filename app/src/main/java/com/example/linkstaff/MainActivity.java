package com.example.linkstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** at sign up click */
    public void signUp(View view) {
//        System.out.println("Clickeeeeeed");
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

}
