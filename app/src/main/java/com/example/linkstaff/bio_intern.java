package com.example.linkstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class bio_intern extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_intern);
    }

    public void bio_intern_next(View view) {
        Intent intent = new Intent(this, Congratulation.class);
        startActivity(intent);
    }
}
