package com.example.linkstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Profile_1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_1);
    }

    public void profile_1__next(View view) {
        Intent intent = new Intent(this, Profile_2.class);
        startActivity(intent);
    }
}
