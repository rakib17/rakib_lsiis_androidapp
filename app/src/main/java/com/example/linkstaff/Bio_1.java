package com.example.linkstaff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Bio_1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_1);
    }

    public void bio_1_next(View view) {
        Intent intent = new Intent(this, Bio_2.class);
        startActivity(intent);
    }
}
